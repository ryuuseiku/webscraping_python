from bs4 import BeautifulSoup
from urllib.request import urlopen
import re
import pandas as pd
import time

# 0.Start scraping
print("Start scraping.")
start_t = time.time()

# 1.get the links of each ship.
base_url = "http://wiki.joyme.com"
category_url = "/blhx/%E8%88%B0%E5%A8%98%E5%9B%BE%E9%89%B4"

html_category = urlopen(base_url + category_url).read().decode('utf-8')

soup = BeautifulSoup(html_category, features='lxml')
ship_links = soup.find_all(
    "a",
    dict(
        href=re.compile(r"(%.{2})+$"),
        title=re.compile(
            r"^([^首页|舰娘定位筛选|驱逐|轻巡|重巡|超巡|战巡|"
            r"战列|航母|航战|轻航|重炮|维修|潜艇])+$")))

# 1-1.filter links
block_list = ["能代", "出云"]
for ship in ship_links:
    result = re.search(r"(.+?)改", ship.get_text())
    if result:
        if ship.get_text() == result.group(0):
            ship_links.remove(ship)
    if ship.get_text() in block_list:
        ship_links.remove(ship)

# 1-2.Prepare frame for saving data
frame = []
property_column = [
    'ship_id',
    'ship_name',
    'ship_class',
    'ship_rarity',
    'ship_camp',
    'ship_armor',
    'ship_max_health',
    'ship_max_reload',
    'ship_max_firepower',
    'ship_max_torpedo',
    'ship_max_evasion',
    'ship_max_antiair',
    'ship_max_aviation',
    'ship_max_oilcost',
    'ship_max_speed',
    'ship_max_luck']
frame.append(property_column)

# 2.Scrap data from each link.
for ship in ship_links:
    # 2-1.open html and feed to BeautifulSoup()
    html_ship = urlopen(base_url + ship.get('href')).read().decode('utf-8')
    soup = BeautifulSoup(html_ship, features='lxml')

    # 2-2.Start to scrap data from the soup
    ship_id = soup.find_all("span", {"id": "PNN"})[0].get_text()
    ship_class = soup.find_all(
        "img", {"data-file-name": re.compile(r"ShipType-(.+?)")})[0].get('alt')
    ship_rarity = soup.find_all("small")[0].get_text()
    ship_camp = soup.find_all(
        "img", {"data-file-name": re.compile(r"Camplogo(.+?)")})[0].get('alt')
    ship_armor = soup.find_all("td", {"style": "width:25%"})[
        1].get_text()[0:-1]

    ship_performance_table = soup.find_all("table", {
        "class": "wikitable sv-performance",
        "style": "width:100%;text-align:center;float:left;margin-top:10px"})
    ship_performance_resultset = []
    if ship_class != "潜艇":
        for tag in ship_performance_table:
            ship_performance_resultset.extend(
                tag.find_all(text=re.compile(r"→(\d+)\n$")))
        ship_max_health = re.search(
            r"(\d+)\n", ship_performance_resultset[0]).group(0).strip('\n')
        ship_max_reload = re.search(
            r"(\d+)\n", ship_performance_resultset[1]).group(0).strip('\n')
        ship_max_firepower = re.search(
            r"(\d+)\n", ship_performance_resultset[2]).group(0).strip('\n')
        ship_max_torpedo = re.search(
            r"(\d+)\n", ship_performance_resultset[3]).group(0).strip('\n')
        ship_max_evasion = re.search(
            r"(\d+)\n", ship_performance_resultset[4]).group(0).strip('\n')
        ship_max_antiair = re.search(
            r"(\d+)\n", ship_performance_resultset[5]).group(0).strip('\n')
        ship_max_aviation = re.search(
            r"(\d+)\n", ship_performance_resultset[6]).group(0).strip('\n')
        ship_max_oilcost = re.search(
            r"(\d+)\n", ship_performance_resultset[7]).group(0).strip('\n')
        ship_max_speed = re.search(
            r"(\d+)\n", ship_performance_resultset[8]).group(0).strip('\n')
        ship_max_luck = re.search(
            r"(\d+)",
            ship_performance_table[0].find_all("tr")[14].find_all(
                text=re.compile(r"(\d+)"))[0]).group(0)

    data_column = [
        ship_id,
        ship.get_text(),
        ship_class,
        ship_rarity,
        ship_camp,
        ship_armor,
        ship_max_health,
        ship_max_reload,
        ship_max_firepower,
        ship_max_torpedo,
        ship_max_evasion,
        ship_max_antiair,
        ship_max_aviation,
        ship_max_oilcost,
        ship_max_speed,
        ship_max_luck]
    frame.append(data_column)

    print(ship.get_text(), "...OK")
frames = pd.DataFrame(frame)
frames.to_csv(
    'e:/WebScraping/MorvanZhou_Tutorials/venv/Data/AzureLane_result01.csv',
    encoding='utf_8_sig')
end_t = time.time()
print("File is Saved.")
print("\nRun Time: ", start_t - end_t)