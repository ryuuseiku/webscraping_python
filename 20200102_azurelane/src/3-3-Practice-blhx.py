import requests
from bs4 import BeautifulSoup
import re

url = "http://wiki.joyme.com/blhx/%E8%88%B0%E5%A8%98%E5%9B%BE%E9%89%B4"
html = requests.get(url).text
soup = BeautifulSoup(html, "lxml")

img_tag = soup.find_all("img", {"alt": re.compile(r"(.+?)头像.jpg")})
for tag in img_tag:
    img_url = tag.get("src")
    img_name = img_url.split("/")[-1]
    img = requests.get(img_url, stream=True)
    with open("e:/WebScraping/MorvanZhou_Tutorials/venv/Pic/blhx_avatar/" + img_name, "wb") as f:
        for chunk in img.iter_content(chunk_size=128):
            f.write(chunk)
    print("saved ", img_name)
